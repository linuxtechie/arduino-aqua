#define Canister  2
#define Food      6
#define AquaHead   4
#define TubeLight 3
#define AirPump   5

#include <Wire.h>
#include <Time.h>
#include <TimeAlarms.h>
#include <DS1307RTC.h>

#define AQUA_DEBUG1 TRUE
#define LIVE       TRUE
tmElements_t tm;
#ifdef LIVE
//unsigned long AirPumpInv   [5] = {6000700, 12001300, 16001700,  21002200,  1000200};
unsigned long AirPumpInv   [5] = {6000630,  10001030,  14001430, 18001830,  22002230};
unsigned long CanisterInv  [4] = {6000700,  12001300,  16001700, 19002000};
//unsigned long FoodInv      [6] = {700,      1035,      1300,     1600,      1900,   2200};
unsigned long FoodInv      [9] = {600,      800,       1000,     1200,      1400,   1600,   1800,   2000,  2200}
unsigned long AquaHeadInv  [4] = {7000715, 12001215, 16001615,  19001915};
unsigned long TubeLightInv [3] = {6001000, 13001500, 18002200};
#else
unsigned long AirPumpInv[1]   = {15001546};
unsigned long CanisterInv[1]  = {15001543};
unsigned long FoodInv[1]      = {2214};
unsigned long AquaHeadInv[1]   = {9001000};
unsigned long TubeLightInv[1] = {15001545};
#endif

#ifdef AQUA_DEBUG
void print2digits(int number) {
  if (number >= 0 && number < 10) {
    Serial.write('0');
  }
  Serial.print(number);
}

void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.println(); 
}

void printDigits(int digits)
{
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}
#endif


void setup() {

  pinMode(Canister,  OUTPUT);
  pinMode(Food,      OUTPUT);
  pinMode(AquaHead,   OUTPUT);
  pinMode(TubeLight, OUTPUT);
  pinMode(AirPump,   OUTPUT);

  digitalWrite(Canister,  HIGH);
  digitalWrite(Food,      HIGH);
  digitalWrite(AquaHead,   HIGH);
  digitalWrite(TubeLight, HIGH);
  digitalWrite(AirPump,   HIGH);
#ifdef AQUA_DEBUG
  Serial.begin(9600);
#endif
  if (RTC.read(tm)) {
#ifdef AQUA_DEBUG
    Serial.print("Ok, Time = ");
    print2digits(tm.Hour);
    Serial.write(':');
    print2digits(tm.Minute);
    Serial.write(':');
    print2digits(tm.Second);
    Serial.print(", Date (D/M/Y) = ");
    Serial.print(tm.Day);
    Serial.write('/');
    Serial.print(tm.Month);
    Serial.write('/');
    Serial.print(tmYearToCalendar(tm.Year));
    Serial.println();
#endif
  } else {
    if (RTC.chipPresent()) {
      Serial.println("The DS1307 is stopped.  Please run the SetTime");
      Serial.println("example to initialize the time and begin running.");
      Serial.println();
    } else {
      Serial.println("DS1307 read error!  Please check the circuitry.");
      Serial.println();
    }
  }
  setTime(tm.Hour, tm.Minute, tm.Second, tm.Month, tm.Day, tm.Year);
}

void checkDevices(unsigned long deviceInv[], int device, int size){
#ifdef AQUA_DEBUG
  Serial.print(device);
  Serial.print("~");
  Serial.println(size);
#endif
  int i = 0;
  bool matched = false;
  time_t currT;
  time_t startT;
  time_t endT;
 
  tmElements_t curr = tm;
  tmElements_t start = tm;
  tmElements_t end = tm;

  start.Second = 0;
  end.Second = 0;
  
  curr.Year = start.Year;
  curr.Month = start.Month;
  curr.Day = start.Day;
  curr.Hour = hour();
  curr.Minute = minute();
  curr.Second = second();


  currT = makeTime(curr);
  
  for (i=0; i < size; i++){
    start.Hour    = (deviceInv[i]/10000)/100;
    start.Minute  = (deviceInv[i]/10000)%100;
    end.Hour      = (deviceInv[i]%10000)/100;
    end.Minute    = (deviceInv[i]%10000)%100;
    
    if (start.Hour == 0 && start.Minute == 0 ){
      start.Hour   = end.Hour;
      start.Minute = end.Minute;
      if ( end.Minute == 0){
        start.Hour--;
        start.Minute = 59;
      }
      else{
        start.Minute--;
      }
    }
    
    startT = makeTime(start);
    endT   = makeTime(end);

    if ( currT >= startT
        && currT <= endT) {
        matched = true;
        break;
    }
  }
  if ( matched ){
    if (digitalRead(device) != LOW)
    {
      digitalWrite(device, LOW);
#ifdef AQUA_DEBUG
      Serial.print("Switching On ");
      Serial.println(device);
      Serial.print(device);
      Serial.print("~");
      Serial.print(start.Hour);
      printDigits(start.Minute);
      Serial.print("--");
      Serial.print(end.Hour);
      printDigits(end.Minute);
      Serial.print("-->");
      Serial.print(curr.Hour);
      printDigits(curr.Minute);
      Serial.println();
#endif
    }
  }
  else{
    if (digitalRead(device) != HIGH)
    {
#ifdef AQUA_DEBUG
      Serial.print("Switching Off ");
      Serial.println(device);
      Serial.print(device);
      Serial.print("~");
      Serial.print(start.Hour);
      printDigits(start.Minute);
      Serial.print("--");
      Serial.print(end.Hour);
      printDigits(end.Minute);
      Serial.print("-->");
      Serial.print(curr.Hour);
      printDigits(curr.Minute);
      Serial.println();
#endif
      digitalWrite(device, HIGH);
    }
  }
}

void deviceLoop(){
  int sLong = sizeof(unsigned long);
  checkDevices(TubeLightInv, TubeLight, sizeof(TubeLightInv)/sLong);
  checkDevices(AirPumpInv,   AirPump,   sizeof(AirPumpInv)/sLong);
  checkDevices(CanisterInv,  Canister,  sizeof(CanisterInv)/sLong);
  checkDevices(AquaHeadInv,  AquaHead,  sizeof(AquaHeadInv)/sLong);
  checkDevices(FoodInv,      Food,      sizeof(FoodInv)/sLong);
}

void loop() {
#ifdef AQUA_DEBUG
  digitalClockDisplay();
#endif
  deviceLoop();
  Alarm.delay(60000);
}



